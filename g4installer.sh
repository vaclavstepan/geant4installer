#!/bin/bash
# 2015-07-29 Updating to 10.2 beta
# 2014-03-14 Vaclav Stepan

# Geant4 version, defines URL and paths
# Retrieve last version number from this URL
DURLS='http://www.geant4.org/geant4/support/download.shtml'
DURLB='http://www.geant4.org/geant4/support/download_beta.shtml'

# Per default use stable 
DURL=$DURLS

# In BASE the geant4.XX.XX source directories will be created 
BASE=${HOME}/geant4

# Where is the build script
G4BUILD=./g4build.sh

# Maybe there is a more logical choice for default base directory
if [ $( basename $( pwd ) ) == geant4 ] ; then
  B=`pwd`
elif [ -d geant4 ] ; then
  B=`pwd`/geant4
fi

# Downloads source and patches it to get last Geant4 with DNA chemistry patch
# Individual parts can be disabled by option switches
function print_help {
        echo Syntax: $0 '[-bpd] [-i <BASEPATH>'
	echo 
        echo Downloads Geant4 source and optionally applies source and DNA patches.
	echo Note: DNA chemistry installation requires repository access.
	echo
	echo Options:
	echo -e \\t' -b '\\t Retrieve beta instead of stable version 
	echo -e \\t' -i <BASEPATH> '\\t Set base directory
	echo -e \\t' -p ' \\t enable Geant4 patch installation
	echo -e \\t' -d ' \\t enable DNA chemistry patch
}

PATCH_URL_OPT=DISABLED;
DNA_GIT_OPT=DISABLED;
while getopts "hbpdi:" opt; do
  case $opt in
	b)
	  # Get beta version instead of the stable one
	  DURL=$DURLB
	  ;;
        p)
          # Disable patch
          PATCH_URL_OPT=ENABLED;
          ;;
        d)
          # Disable DNA chemistry patching
          DNA_GIT_OPT=ENABLED;
          ;;
	i)
	  # Target directory for installation 
	  BASE="$OPTARG"
	  ;;
        h)
          print_help;
	  exit 0
          ;;
        \?)
          echo "Invalid option: -$OPTARG" >&2
          ;;
  esac
done

# Prepare the paths etc.
GV=$( basename $( wget -q -O - "$DURL" | grep -E '"geant4.*.tar.gz' | cut -f 2 -d \" ) .tar.gz )
#GV=geant4.10.01.p02

# Current dir with Geant4 sources
URL=http://geant4.cern.ch/support/source/${GV}.tar.gz
# Not used unless command line option given
PATCH_URL=http://geant4.cern.ch/support/source/patch_geant4.10.00.p01.tar.gz

# You need repository access for this to work
DNA_GIT=git@bitbucket.org:matkara/dna.git

# --------------------
BN=$( basename "$URL" )

SRC=$( basename $BN .tar.gz )

time (
echo ------------------------------------------------------------
echo Installer starting at: $( date )
echo ------------------------------------------------------------

echo Configuration:
echo -e "\t Geant4 source:  \t $URL"
echo -e "\t Source patches: \t $PATCH_URL"
echo -e "\t DNA Git tree:   \t $DNA_GIT"
echo
echo -e "\t Base directory: \t $BASE"
echo

mkdir -p $BASE && cd $BASE

if [ -d $SRC ] ; then
     echo $SRC dir already exists. Unsure about what you ask for.
     exit 1
else 
     echo Retrieving and unpacking Geant4 source...
     wget -O - $URL | tar xz || exit 1
fi

echo
echo ------------------------------------------------------------
if [ "${PATCH_URL_OPT}" == "DISABLED" ] ; then 
	echo Not installing patches.
	echo ------------------------------------------------------------
else
	echo Retrieving and installing Geant4 patches
	echo ------------------------------------------------------------
	PATCH_BN=$( basename "$PATCH_URL" )
	wget -O - $PATCH_URL | tar xz || exit 1
	echo Renaming the ${SRC} to ${SRC}-p
	mv -v ${BASE}/${SRC} ${BASE}/${SRC}-p
	SRC=${SRC}-p
fi

echo ------------------------------------------------------------
if [ "${DNA_GIT_OPT}" == "DISABLED" ] ; then
	echo Not installing DNA-chemistry.
	echo ------------------------------------------------------------
else
	echo
	echo Retrieving last DNA-chemistry version
	echo ------------------------------------------------------------
	
	if [ -d ${SRC}/source/processes/electromagnetic/dna ] ; then
	  cd ${SRC}/source/processes/electromagnetic 
	  rm -rf dna &&
	  git clone ${DNA_GIT} &&
	  cd dna && 
	  git checkout v10cmakefiles 
	  echo ------------------------------------------------------------
	  git status
	  git remote -v 
	else
	  echo ERROR: Not a directory ${SRC}/source/processes/electromagnetic/dna.
	  exit 1
	fi
	echo Renaming the ${SRC} to ${SRC}-d
	mv -v ${BASE}/${SRC} ${BASE}/${SRC}-d
	export SRC=${SRC}-d
fi

echo
echo ------------------------------------------------------------
echo
echo To build the code, you can use:
echo -e \\t $G4BUILD ${BASE}/${SRC} 
echo To build for MT mode, do:
echo -e \\t $G4BUILD ${BASE}/${SRC} --mt
echo 

)
