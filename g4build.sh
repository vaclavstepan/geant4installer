#!/bin/bash
# Builds the Geant4 
# 2015-07-29 Vaclav Stepan

# Adjust:
NUMTHREADS=$( nproc )

# Default base directory
B=${HOME}/geant4

# Limited magic: If we are in geant4 dir or there is a geant4 subdir, let us use it
if [ $( basename $( pwd ) ) == geant4 ] ; then
  B=`pwd`
elif [ -d geant4 ] ; then
  B=`pwd`/geant4
fi

# Source directory
SRC=$1

echo Base directory: $B

if [ x$SRC = x ] ; then
    echo Specify source directory as argument
    exit 1
else
    if [ ! -d $SRC ] ; then
	echo Source not a directory
    else
	echo Using $SRC as source  
    fi 
fi



#------------------------------------------------------------
BUILD=${SRC}-build

if [ x$2 == x'--mt' ]; then
  C_MT='-DGEANT4_BUILD_MULTITHREADED=ON'
  BUILD=${BUILD}-mt
  INSTALL_SUFFIX='-mt'
fi

if [ -d $BUILD ] ; then
   echo Note: $BUILD build directory already exists.
else
   mkdir -p $BUILD || exit 1
fi

echo Building source $SRC in $BUILD
cd $BUILD || exit 1

INSTALL_DIR=${B}/$(basename ${SRC})-install${INSTALL_SUFFIX}

#Adjust the settings as requested 
cmake 	-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
	-DCMAKE_BUILD_TYPE=RelWithDebInfo \
	-DGEANT4_INSTALL_DATA=ON \
	-DGEANT4_INSTALL_DATADIR=${B}/data \
	-DGEANT4_USE_OPENGL_X11=ON \
	${C_MT} \
	-DGEANT4_USE_QT=ON \
	-DGEANT4_USE_RAYTRACER_X11=ON \
	-DGEANT4_USE_XM=ON \
	../`basename $SRC` || exit 1

time make -j $NUMTHREADS || exit 1

time make install || exit 1
